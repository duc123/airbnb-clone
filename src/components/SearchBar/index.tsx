import * as React from "react";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";
import style from "./style.module.scss";

const SearchBar = () => {
  return (
    <div className={style.container}>
      <span className={style.item}>Any where</span>
      <div className={style.line} />
      <span className={style.item}>Any week</span>
      <div className={style.line} />

      <span className={style.item}>Add guests</span>
      <span className={style.icon}>
        <SearchOutlinedIcon />
      </span>
    </div>
  );
};

export default SearchBar;
