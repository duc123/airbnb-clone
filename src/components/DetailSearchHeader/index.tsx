/* eslint-disable @typescript-eslint/no-unused-vars */
import { LocationOnOutlined, SearchOutlined } from "@mui/icons-material";
import * as React from "react";
import { useSelector } from "react-redux";
import { useAppDispatch } from "../../app/hooks";
import { searchLocationInput } from "../../app/services/util.service";
import { selectWhere } from "../../redux/search/search.selector";
import { updateWhere } from "../../redux/search/search.slice";
import { selectSearchActive } from "../../redux/util/util.selector";
import AddGuests from "../AddGuests";
import PickDate from "../PickDate";
import style from "./style.module.scss";

const DetailSearchHeader = () => {
  const dispatch = useAppDispatch();

  const searchBarRef: any = React.useRef(null);

  const [searchText, setSearchText] = React.useState("");

  const searchActive = useSelector(selectSearchActive);
  const where = useSelector(selectWhere);

  const [active, setActive] = React.useState("where");

  const [locations, setLocations] = React.useState<any>([
    {
      id: 1,
      display_name: "duc",
    },

    {
      id: 2,
      display_name: "sdads",
    },
    {
      id: 3,
      display_name: "sdads",
    },
    {
      id: 4,
      display_name: "sdads",
    },
    {
      id: 5,
      display_name: "sdads",
    },
    {
      id: 6,
      display_name: "sdads",
    },
  ]);

  const [locationShown, setLocationShown] = React.useState(false);

  const regions = [
    "Europe",
    "Asia",
    "South America",
    "United States",
    "France",
    "United Kingdom",
  ];

  const handleActive = (name: string) => {
    return () => {
      setActive(name);
    };
  };

  const getClass = (name: string) => {
    let result = style.searchBar_item_box;
    if (!active) return result;
    if (active === name) {
      result = `${style.searchBar_item_box} ${style.active}`;
    } else {
      result = `${style.searchBar_item_box} ${style.has}`;
    }
    return result;
  };

  const onSearch = () => {
    console.log("Search");
  };

  const onClickOutSide = (e: MouseEvent) => {
    if (searchBarRef.current)
      if (!searchBarRef.current.contains(e.target)) setActive("");
  };

  const searchLocation = async (text: string) => {
    if (!text) return;
    try {
      //   const res = await searchLocationInput(text);
      //   const { autocomplete_terms } = res;
      //   if (autocomplete_terms) setLocations(autocomplete_terms);
    } catch (err) {
      console.log(err);
    }
  };

  const onSearchLocation = (e: any) => {
    setLocationShown(() => true);
    setSearchText(() => e.target.value);
  };

  const handleBlur = () => {
    if (!searchText) {
      dispatch(updateWhere(""));
      return;
    }
    setSearchText(() => locations[0].display_name);
  };

  const onChoseLocation = (name: string) => {
    return () => {
      dispatch(updateWhere(name));
      setActive(() => "check in");
    };
  };

  React.useEffect(() => {
    const time = setTimeout(searchLocation, 300, searchText);

    return () => clearTimeout(time);
  }, [searchText]);

  React.useEffect(() => {
    if (searchBarRef.current) {
      document.addEventListener("mousedown", onClickOutSide);
    }
    return () => document.removeEventListener("mousedown", onClickOutSide);
  }, [searchBarRef]);

  React.useEffect(() => {
    setSearchText(where);
  }, [where]);

  return (
    <div className={`${style.container} ${searchActive ? style.active : ""}`}>
      <div
        ref={searchBarRef}
        className={`${style.searchBar} ${active ? style.active : ""}`}
      >
        <div className={style.searchBar_item}>
          <div onClick={handleActive("where")} className={getClass("where")}>
            <span className={style.title}>Where</span>
            <input
              onBlur={handleBlur}
              value={searchText}
              onChange={onSearchLocation}
              placeholder="Search destinations"
            />
          </div>

          {active === "where" && (
            <div className={`${style.where}`}>
              {locationShown ? (
                <div className={style.locations}>
                  {locations.map((el: any) => (
                    <div
                      onClick={onChoseLocation(el.display_name)}
                      className={style.location}
                      key={el.id}
                    >
                      <span className={style.loIcon}>
                        <LocationOnOutlined />
                      </span>
                      <span className={style.loText}>{el.display_name}</span>
                    </div>
                  ))}
                </div>
              ) : (
                <div className={style.regionSelect}>
                  <h5>Search by region</h5>
                  <div className={style.regions}>
                    {regions.map((el) => (
                      <div key={el} className={style.region}>
                        <div
                          onClick={onChoseLocation(el)}
                          className={style.imgContainer}
                        >
                          <img
                            src="https://a0.muscache.com/pictures/f9ec8a23-ed44-420b-83e5-10ff1f071a13.jpg"
                            alt=""
                          />
                        </div>
                        <span>{el}</span>
                      </div>
                    ))}
                  </div>
                </div>
              )}
            </div>
          )}
        </div>

        <div
          onClick={handleActive("check in")}
          className={style.searchBar_item}
        >
          <div className={getClass("check in")}>
            <span className={style.title}>Check in</span>
            <span>Add date</span>
          </div>

          {active === "check in" && (
            <div className={`${style.checkIn} ${style.pickDate}`}>
              <PickDate />
            </div>
          )}
        </div>

        <div
          onClick={handleActive("check out")}
          className={style.searchBar_item}
        >
          <div className={getClass("check out")}>
            <span className={style.title}>Check out</span>
            <span>Add date</span>
          </div>
          {active === "check out" && (
            <div className={`${style.checkOut} ${style.pickDate}`}>
              <PickDate />
            </div>
          )}
        </div>
        <div className={style.searchBar_item}>
          {active === "who" && (
            <div className={`${style.who}`}>
              <AddGuests />
            </div>
          )}
          <div className={getClass("who")}>
            <div onClick={handleActive("who")}>
              <span className={style.title}>Who</span>
              <span>Add guests</span>
            </div>
            <span
              onClick={onSearch}
              className={`${style.searchIcon} ${active ? style.active : ""}`}
            >
              <SearchOutlined />
              {active && "Search"}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailSearchHeader;
