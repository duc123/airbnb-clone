/* eslint-disable @typescript-eslint/no-unused-vars */
import * as React from "react";
import MenuOutlinedIcon from "@mui/icons-material/MenuOutlined";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
// import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";
import style from "./style.module.scss";
import AccountMenuList from "../AccountMenuList";

const AccountMenu = () => {
  const [active, setActive] = React.useState(true);
  const btnRef = React.useRef(null);

  const hideList = () => {
    setActive(() => false);
  };
  const showList = () => {
    if (!active) {
      setActive(true);
      return;
    }
    hideList();
  };
  React.useEffect(() => {
    hideList();
    return () => hideList();
  }, []);

  return (
    <div className={style.wrap}>
      <div
        ref={btnRef}
        onClick={showList}
        className={`${style.container} ${active && style.active}`}
      >
        <MenuOutlinedIcon />
        <span className={style.icon}>
          <AccountCircleOutlinedIcon />
        </span>
      </div>
      <AccountMenuList btnRef={btnRef} hideList={hideList} active={active} />
    </div>
  );
};

export default AccountMenu;
