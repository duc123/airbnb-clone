import * as React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import LinkOutlinedIcon from "@mui/icons-material/LinkOutlined";
import CustomizedButton from "../CustomizedButton";
import AccountMenu from "../AccountMenu";
import SearchBar from "../SearchBar";
import style from "./style.module.scss";
import CategoryList from "../CategoryList";
import useScrollPastOne from "../../hooks/useScrollPast1";
import DetailSearchHeader from "../DetailSearchHeader";
import { useAppDispatch } from "../../app/hooks";
import { selectSearchActive } from "../../redux/util/util.selector";
import { openSearch } from "../../redux/util/util.slice";

interface props {
  searchBarVisible?: boolean;
  filterVisible?: boolean;
}

const Header = ({ searchBarVisible, filterVisible }: props) => {
  const [pastOne] = useScrollPastOne();

  const dispatch = useAppDispatch();

  const searchActive = useSelector(selectSearchActive);

  const onSearchDetail = () => {
    dispatch(openSearch());
  };

  return (
    <>
      <header className={`${style.header} `}>
        {searchActive && <div className={style.outskirt} />}
        <div className={style.header_border}>
          <div className={style.container}>
            <div className={style.container_flex}>
              <div className={`${style.part} ${style.left}`}>
                <Link className={style.title} to="/">
                  <span>WeHome</span>
                </Link>
              </div>
              <div className={`${style.part} ${style.center}`}>
                {searchBarVisible && (
                  <>
                    <div
                      className={`${style.searchDetail} ${
                        searchActive ? style.active : ""
                      }`}
                    >
                      <span>Stays</span>
                      <span>Experiences</span>
                      <span>Online experiences</span>
                    </div>
                    <div
                      className={`${style.searchBarContainer} ${
                        searchActive ? style.active : ""
                      }`}
                    >
                      <div style={{ width: "100%" }} onClick={onSearchDetail}>
                        {searchActive ? <DetailSearchHeader /> : <SearchBar />}
                      </div>
                    </div>
                  </>
                )}
              </div>
              <div className={`${style.part} ${style.right}`}>
                <CustomizedButton
                  onClick={null}
                  href="/become-a-host"
                  loading={false}
                  type="button"
                  fullwidth={false}
                  title="Become a host"
                  disabled={false}
                  size="medium"
                  color="inherit"
                  variant="text"
                />
                <AccountMenu />
              </div>
            </div>
          </div>
        </div>
      </header>
      {filterVisible && !searchActive && (
        <div className={`${pastOne ? style.past : ""} ${style.filters}`}>
          <div className={style.container}>
            <div className={style.categories}>
              <div className={style.categories_wrap}>
                <div className={style.categories_wrap_list}>
                  <CategoryList />
                </div>
              </div>
              <span className={style.filterBtn}>
                <LinkOutlinedIcon />
                <span>Filter</span>
              </span>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

Header.defaultProps = {
  searchBarVisible: false,
  filterVisible: false,
};

export default Header;
