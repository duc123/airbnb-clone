import SearchOutlined from "@mui/icons-material/SearchOutlined";
import LinkOutlinedIcon from "@mui/icons-material/LinkOutlined";
import * as React from "react";
import CategoryList from "../CategoryList";
import style from "./style.module.scss";

const MobileHeader = () => {
  return (
    <div className={style.container}>
      <div className={style.content}>
        <div style={{ width: "100%" }}>
          <div className={style.searchBar}>
            <div className={style.searchBar_left}>
              <SearchOutlined />
              <div className={style.searchBar_left_questions}>
                <span className={style.qTitle}>Where to?</span>
                <div className={style.values}>
                  <span>Any where</span>
                  <span>Any week</span>
                  <span>Add guests</span>
                </div>
              </div>
            </div>
            <span className={style.filterIcon}>
              <LinkOutlinedIcon />
            </span>
          </div>
          <div className={style.filterBox}>
            <CategoryList />
          </div>
        </div>
      </div>
    </div>
  );
};

export default MobileHeader;
