import * as React from "react";
import moment from "moment";
import Calendar from "react-calendar";
import { IconButton } from "@mui/material";
import {
  NavigateBeforeOutlined,
  NavigateNextOutlined,
} from "@mui/icons-material";
import style from "./style.module.scss";

const PickDate = () => {
  return (
    <div className={style.container}>
      <Calendar
        nextLabel={
          <IconButton>
            <NavigateNextOutlined />
          </IconButton>
        }
        prevLabel={
          <IconButton>
            <NavigateBeforeOutlined />
          </IconButton>
        }
        className={style.calendars}
        showDoubleView
        locale="en"
        navigationLabel={({ date }) =>
          `${moment(date).format("MMMM")} ${date.getFullYear()}`
        }
        next2Label=""
        minDate={new Date()}
      />
    </div>
  );
};

export default PickDate;
