/* eslint-disable react/jsx-boolean-value */
/* eslint-disable react/no-array-index-key */
import * as React from "react";
import KeyboardArrowLeftOutlinedIcon from "@mui/icons-material/KeyboardArrowLeftOutlined";
import KeyboardArrowRightOutlinedIcon from "@mui/icons-material/KeyboardArrowRightOutlined";
import CasesOutlinedIcon from "@mui/icons-material/CasesOutlined";
import { Swiper, SwiperSlide } from "swiper/react";
import style from "./style.module.scss";
import "swiper/scss";

const CategoryList = () => {
  const swipe = React.useRef({
    slideNext: () => {},
    slidePrev: () => {},
  });
  const list = [];

  for (let i = 0; i < 25; i += 1) {
    list.push("hello");
  }

  const onNext = () => {
    // console.log(swipe.current);

    swipe.current.slideNext();
  };

  const onPrev = () => {
    swipe.current.slidePrev();
  };

  return (
    <div className={style.container}>
      <span className={style.next} onClick={onPrev}>
        <KeyboardArrowLeftOutlinedIcon />
      </span>
      <Swiper
        breakpoints={{
          768: {
            slidesPerView: 7,
          },
          1024: {
            slidesPerView: 8,
          },
          1200: {
            slidesPerView: 10,
          },
        }}
        onSwiper={(swiper) => {
          swipe.current = swiper;
        }}
        slidesPerView={5}
      >
        {list.map((el, i) => (
          <SwiperSlide className={style.item} key={i}>
            <div className={style.item_box}>
              <CasesOutlinedIcon />
              <span>{el}</span>
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
      <span className={style.prev} onClick={onNext}>
        <KeyboardArrowRightOutlinedIcon />
      </span>
    </div>
  );
};

export default CategoryList;
