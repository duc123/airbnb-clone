import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import * as React from "react";
import style from "./style.module.scss";

interface props {
  fullWidth?: boolean;
  label: string;
  value: string | undefined;
  onChange: any;
  options: string[];
  disabled?: boolean;
  error?: boolean;
  errorText?: string;
  onBlur?: any;
}

const CustomizedSelect = ({
  fullWidth,
  label,
  value,
  onChange,
  options,
  disabled,
  error,
  errorText,
  onBlur,
}: props) => {
  return (
    <FormControl error={error} disabled={disabled} fullWidth={fullWidth}>
      <InputLabel id="demo-simple-select-label">{label}</InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={value}
        label={label}
        onChange={onChange}
        onBlur={onBlur}
      >
        {options.map((item) => (
          <MenuItem key={item} value={item}>
            {item}
          </MenuItem>
        ))}
      </Select>
      {errorText && <small className={style.error}>{errorText}</small>}
    </FormControl>
  );
};

CustomizedSelect.defaultProps = {
  fullWidth: false,
  disabled: false,
  error: false,
  errorText: "",
  onBlur: () => {},
};

export default CustomizedSelect;
