import { PriorityHighOutlined } from "@mui/icons-material";
import * as React from "react";
import style from "./style.module.scss";

interface propInterface {
  sum: string | undefined;
  detail: string;
  color: "error" | "success";
}

const CustomizedAlert = ({ sum, detail, color }: propInterface) => {
  return (
    <div className={`${style.container} ${style[color]}`}>
      <span className={style.icon}>
        <PriorityHighOutlined />
      </span>
      <div className={style.container_content}>
        <span className={style.sum}>{sum}</span>
        <span className={style.detail}>{detail}</span>
      </div>
    </div>
  );
};

export default CustomizedAlert;
