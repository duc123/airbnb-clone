import {
  AccountBoxOutlined,
  ChatOutlined,
  FavoriteOutlined,
  LoginOutlined,
  LogoutOutlined,
} from "@mui/icons-material";
import SearchOutlined from "@mui/icons-material/SearchOutlined";
import * as React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { useAppDispatch } from "../../app/hooks";
import useScrollingUp from "../../hooks/useScrollingUp";
import { logoutRequest } from "../../redux/login/login.actions";
import { selectLoginUid } from "../../redux/login/login.selector";
import ConfirmModal from "../ConfirmModal";
import style from "./style.module.scss";

const MobileFooterNav = () => {
  const uid = useSelector(selectLoginUid);

  const scrollUp = useScrollingUp();

  const [visible, setVisible] = React.useState(false);

  const dispatch = useAppDispatch();

  const guestList = [
    {
      title: "Search",
      url: "/",
      icon: <SearchOutlined />,
      isBtn: false,
    },
    {
      title: "Wish list",
      url: "wish-list",
      icon: <FavoriteOutlined />,
      isBtn: false,
    },
    {
      title: "Login",
      url: "login",
      icon: <LoginOutlined />,
      isBtn: false,
    },
  ];

  const userList = [
    {
      title: "Search",
      url: "/",
      icon: <SearchOutlined />,
      isBtn: false,
    },
    {
      title: "Wish list",
      url: "wish-list",
      icon: <FavoriteOutlined />,
      isBtn: false,
    },
    {
      title: "Inbox",
      url: "inbox",
      icon: <ChatOutlined />,
      isBtn: false,
    },

    {
      title: "Account",
      url: "account",
      icon: <AccountBoxOutlined />,
      isBtn: false,
    },
    {
      title: "Logout",
      url: "",
      icon: <LogoutOutlined />,
      isBtn: true,
    },
  ];

  const onLogout = () => {
    dispatch(logoutRequest());
  };

  const openModal = () => setVisible(() => true);

  const closeModal = () => setVisible(() => false);

  const renderList = () => {
    const list = uid ? userList : guestList;
    return list.map((el) =>
      el.isBtn ? (
        <div onClick={openModal} key={el.url} className={style.item}>
          {el.icon}
          <span>{el.title}</span>
        </div>
      ) : (
        <NavLink
          key={el.url}
          to={`${el.url}`}
          className={(state) => {
            return `${style.item} ${state.isActive ? style.active : ""}`;
          }}
        >
          {el.icon}
          <span>{el.title}</span>
        </NavLink>
      ),
    );
  };

  return (
    <div className={`${style.container} ${scrollUp ? style.active : ""}`}>
      <div className={style.content}>{renderList()}</div>

      <ConfirmModal
        open={visible}
        closeModal={closeModal}
        title="Logout confirm"
        detail="Are you sure you want to logout?"
        callback={onLogout}
      />
    </div>
  );
};

export default MobileFooterNav;
