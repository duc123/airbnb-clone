import * as React from "react";
import { useSelector } from "react-redux";
import { useAppDispatch } from "../../app/hooks";
import { getAuthInfo } from "../../redux/login/login.actions";
import { selectLoginUid } from "../../redux/login/login.selector";
import { getCountryList } from "../../redux/util/util.actions";
import { selectSearchActive } from "../../redux/util/util.selector";
import { closeSearch } from "../../redux/util/util.slice";
import style from "./style.module.scss";

type Props = {
  children: React.ReactNode;
};
const Wrapper: React.FC<Props> = ({ children }) => {
  const uid = useSelector(selectLoginUid);

  const dispatch = useAppDispatch();

  const searchActive = useSelector(selectSearchActive);

  const handleClose = () => {
    dispatch(closeSearch());
  };

  React.useEffect(() => {
    if (uid) {
      dispatch(getAuthInfo(uid));
      dispatch(getCountryList());
    }
  }, [uid]);

  return (
    <div>
      {searchActive && <div onClick={handleClose} className={style.outskirt} />}
      {children}
    </div>
  );
};

export default Wrapper;
