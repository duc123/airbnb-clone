/* eslint-disable @typescript-eslint/no-unused-vars */
import { Modal } from "@mui/material";
import * as React from "react";
import CustomizedButton from "../CustomizedButton";
import style from "./style.module.scss";

interface propInterface {
  open: boolean;
  closeModal: Function;
  title: string;
  detail: string;
  callback: Function | null;
}

const ConfirmModal = ({
  open,
  closeModal,
  title,
  detail,
  callback,
}: propInterface) => {
  const onCancel = () => {
    closeModal();
  };

  const onOk = async () => {
    if (callback) {
      await callback();
      closeModal();
    }
  };

  return (
    <Modal onClose={onCancel} open={open}>
      <div className={style.content}>
        <div className={style.content_head}>
          <span className={style.title}>{title}</span>
        </div>
        <div className={style.content_body}>
          <span className={style.detail}>{detail}</span>
        </div>
        <div className={style.content_footer}>
          <CustomizedButton
            onClick={onCancel}
            href=""
            loading={false}
            type="button"
            fullwidth={false}
            title="Cancel"
            disabled={false}
            size="medium"
            color="inherit"
            variant="contained"
          />
          <CustomizedButton
            onClick={onOk}
            href=""
            loading={false}
            type="button"
            fullwidth={false}
            title="Ok"
            disabled={false}
            size="medium"
            color="primary"
            variant="contained"
          />
        </div>
      </div>
    </Modal>
  );
};

export default ConfirmModal;
