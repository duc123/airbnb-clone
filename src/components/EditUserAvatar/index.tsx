import { LoadingButton } from "@mui/lab";
import { Button } from "@mui/material";
import * as React from "react";
import { useSelector } from "react-redux";
import { useAppDispatch } from "../../app/hooks";
import { updateAuthAvatar } from "../../redux/login/login.actions";
import {
  selectAuthInfo,
  selectAvatarLoading,
  selectLoginUid,
} from "../../redux/login/login.selector";
// import CustomizedButton from "../CustomizedButton";
import style from "./style.module.scss";

const Loading = () => {
  return <div>Loading</div>;
};

const EditUserAvatar = () => {
  const authInfo = useSelector(selectAuthInfo);
  const loading = useSelector(selectAvatarLoading);

  const id = useSelector(selectLoginUid);

  const dispatch = useAppDispatch();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files && e.target.files?.length > 0) {
      const file = e.target.files[0];
      if (id)
        dispatch(updateAuthAvatar({ id, file, oldAvatar: authInfo?.avatar }));
    }
  };

  const display = () => {
    return authInfo?.avatar ? (
      <img src={authInfo?.avatar} alt="" />
    ) : (
      <img
        alt="muscahe"
        src="https://a0.muscache.com/defaults/user_pic-225x225.png?v=3"
      />
    );
  };

  return authInfo ? (
    <div className={style.container}>
      <div className={style.edit}>
        <div className={style.imgContainer}>
          {loading ? <LoadingButton loading /> : display()}
        </div>
        {/* <input type="file"/> */}
        <Button disableElevation variant="text" component="label">
          Upload photo
          <input onChange={handleChange} hidden accept="image/*" type="file" />
        </Button>
        {/* <CustomizedButton variant="text" title="Edit photo" type="button" /> */}
      </div>
      <div className={style.content}>
        <h3>Identity verification</h3>
        <p>
          Having a photo will help you connect with other people more easily
        </p>
      </div>
    </div>
  ) : (
    <Loading />
  );
};

export default EditUserAvatar;
