import React, { useState } from "react";
import * as yup from "yup";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { VisibilityOffOutlined, VisibilityOutlined } from "@mui/icons-material";
import { useNavigate } from "react-router-dom";

import { useAppDispatch, useAppSelector } from "../../app/hooks";
import AuthInput from "../AuthInput";
import CustomizedButton from "../CustomizedButton";
import style from "./style.module.scss";
import CustomizedAlert from "../CustomizedAlert";
import {
  selectRegisterError,
  selectRegisterLoading,
} from "../../redux/register/register.selector";
import { registerRequest } from "../../redux/register/register.actions";

const RegisterForm: React.FC<{}> = () => {
  const [passType, setPassType] = useState("password");
  const dispatch = useAppDispatch();
  const loading = useAppSelector(selectRegisterLoading);
  const serverError = useAppSelector(selectRegisterError);

  const onChangePassType = () =>
    setPassType(() => (passType === "text" ? "password" : "text"));

  const navigate = useNavigate();

  const defaultValues = {
    firstname: "",
    lastname: "",
    email: "",
    password: "",
  };

  const schema = yup.object({
    firstname: yup.string().required("Firstname is required"),
    lastname: yup.string().required("Lastname is required"),

    email: yup
      .string()
      .required("Email is required")
      .email("Please enter an invalid email"),
    password: yup.string().required("Password is required"),
  });

  const { handleSubmit, control } = useForm({
    resolver: yupResolver(schema),
    defaultValues,
    mode: "all",
  });

  const submit = async (data: any) => {
    const res = await dispatch(registerRequest(data));
    if (res.payload) navigate("/");
  };

  return (
    <div className={style.authForm}>
      <form onSubmit={handleSubmit(submit)}>
        {serverError && (
          <div style={{ marginBottom: "20px" }}>
            <CustomizedAlert
              sum="Login error"
              detail={serverError}
              color="error"
            />
          </div>
        )}

        <h3> Register new account</h3>
        <div className={style.formControl}>
          <Controller
            render={({
              field: { value, onChange, onBlur },
              fieldState: { error },
            }) => {
              return (
                <AuthInput
                  onBlur={onBlur}
                  iconClick={null}
                  endAdornment=""
                  disabled={false}
                  // eslint-disable-next-line no-unneeded-ternary
                  error={error ? true : false}
                  value={value}
                  onChange={onChange}
                  label="Firstname"
                  size="medium"
                  color="primary"
                  fullWidth
                  type="text"
                  errorText={error?.message}
                />
              );
            }}
            name="firstname"
            control={control}
          />
        </div>
        <div className={style.formControl}>
          <Controller
            render={({
              field: { value, onChange, onBlur },
              fieldState: { error },
            }) => {
              return (
                <AuthInput
                  onBlur={onBlur}
                  iconClick={null}
                  endAdornment=""
                  disabled={false}
                  // eslint-disable-next-line no-unneeded-ternary
                  error={error ? true : false}
                  value={value}
                  onChange={onChange}
                  label="Lastname"
                  size="medium"
                  color="primary"
                  fullWidth
                  type="text"
                  errorText={error?.message}
                />
              );
            }}
            name="lastname"
            control={control}
          />
        </div>
        <div className={style.formControl}>
          <Controller
            render={({
              field: { value, onChange, onBlur },
              fieldState: { error },
            }) => {
              return (
                <AuthInput
                  onBlur={onBlur}
                  iconClick={null}
                  endAdornment=""
                  disabled={false}
                  // eslint-disable-next-line no-unneeded-ternary
                  error={error ? true : false}
                  value={value}
                  onChange={onChange}
                  label="Email"
                  size="medium"
                  color="primary"
                  fullWidth
                  type="text"
                  errorText={error?.message}
                />
              );
            }}
            name="email"
            control={control}
          />
        </div>
        <div className={style.formControl}>
          <Controller
            render={({
              field: { value, onChange, onBlur },
              fieldState: { error },
            }) => {
              return (
                <AuthInput
                  onBlur={onBlur}
                  iconClick={onChangePassType}
                  endAdornment={
                    passType === "text" ? (
                      <VisibilityOffOutlined />
                    ) : (
                      <VisibilityOutlined />
                    )
                  }
                  disabled={false}
                  // eslint-disable-next-line no-unneeded-ternary
                  error={error ? true : false}
                  value={value}
                  onChange={onChange}
                  label="Password"
                  size="medium"
                  color="primary"
                  fullWidth
                  type={passType}
                  errorText={error?.message}
                />
              );
            }}
            name="password"
            control={control}
          />
        </div>
        <CustomizedButton
          onClick={null}
          href=""
          loading={loading}
          type="submit"
          fullwidth
          title="Register"
          disabled={false}
          size="large"
          color="primary"
          variant="contained"
        />
      </form>
    </div>
  );
};

export default RegisterForm;
