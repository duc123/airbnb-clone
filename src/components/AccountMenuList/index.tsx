import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { useAppDispatch } from "../../app/hooks";
import { logoutRequest } from "../../redux/login/login.actions";
import { selectLoginUid } from "../../redux/login/login.selector";
import ConfirmModal from "../ConfirmModal";
import style from "./style.module.scss";

interface propInterface {
  active: boolean;
  hideList: Function;
  btnRef: any;
}

const AccountMenuList = ({ active, hideList, btnRef }: propInterface) => {
  const ref: any = React.useRef(null);
  const uid = useSelector(selectLoginUid);

  const [visible, setVisible] = React.useState(false);

  const openModal = () => {
    hideList();
    setVisible(true);
  };
  const closeModal = () => {
    setVisible(false);
  };
  const dispatch = useAppDispatch();

  const onClickOutside = (e: MouseEvent) => {
    if (ref.current && btnRef.current) {
      if (
        !ref.current.contains(e.target) &&
        !btnRef.current.contains(e.target)
      ) {
        hideList();
        document.removeEventListener("mousedown", onClickOutside);
      }
    }
  };

  const onLogout = () => {
    dispatch(logoutRequest());
  };

  const clickItem = () => {
    hideList();
  };

  useEffect(() => {
    if (active) {
      if (ref.current && btnRef) {
        document.addEventListener("mousedown", (e) => {
          onClickOutside(e);
        });
      }
    }
    return () => {
      document.removeEventListener("mousedown", onClickOutside);
    };
  }, [ref, active]);

  return (
    <div ref={ref} className={`${style.container} ${active && style.active}`}>
      {uid ? (
        <>
          <Link onClick={clickItem} to="/account" className={`${style.item}`}>
            Account
          </Link>
          <Link onClick={clickItem} to="/" className={`${style.item}`}>
            Messages
          </Link>
          <Link onClick={clickItem} to="/" className={`${style.item}`}>
            Notifications
          </Link>
        </>
      ) : (
        <>
          <Link onClick={clickItem} to="/login" className={`${style.item}`}>
            Login
          </Link>
          <Link onClick={clickItem} to="/register" className={`${style.item}`}>
            Register
          </Link>
        </>
      )}
      <div className={`${style.line}`} />
      <span className={`${style.item}`}>Host your experience</span>

      <span className={`${style.item}`}>Host your home</span>

      <span className={`${style.item}`}>Help</span>

      {uid && (
        <>
          <div className={`${style.line}`} />

          <ConfirmModal
            open={visible}
            closeModal={closeModal}
            title="Logout confirm"
            detail="Are you sure you want to logout?"
            callback={onLogout}
          />
          <span onClick={openModal} className={`${style.item}`}>
            Logout
          </span>
        </>
      )}
    </div>
  );
};

export default AccountMenuList;
