import {
  FormControl,
  IconButton,
  InputLabel,
  OutlinedInput,
} from "@mui/material";
import React, { ChangeEventHandler } from "react";
import style from "./style.module.scss";

interface PropInterface {
  color?:
    | "error"
    | "primary"
    | "secondary"
    | "info"
    | "success"
    | "warning"
    | undefined;
  disabled?: boolean;
  error?: boolean;
  fullWidth?: boolean;
  //   label: string;
  onChange: ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>;
  onBlur: ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>;
  size?: "medium" | "small" | undefined;
  value: any;
  type: string;
  errorText?: string | undefined;
  label: string;
  endAdornment?: any;
  iconClick?: any;
  multiline?: boolean;
  rows?: number;
}

const AuthInput = ({
  color,
  disabled,
  error,
  fullWidth,
  onChange,
  size,
  value,
  type,
  errorText,
  label,
  iconClick,
  endAdornment,
  onBlur,
  multiline,
  rows,
}: PropInterface) => {
  const onIconClick = () => {
    if (iconClick) iconClick();
  };

  return (
    <FormControl error={error} sx={{ width: "100%" }} variant="outlined">
      <InputLabel htmlFor={label}>{label}</InputLabel>

      <OutlinedInput
        rows={rows}
        multiline={multiline}
        endAdornment={
          endAdornment ? (
            <IconButton onClick={onIconClick}>{endAdornment}</IconButton>
          ) : (
            ""
          )
        }
        id={label}
        disabled={disabled}
        color={color}
        fullWidth={fullWidth}
        onChange={onChange}
        onBlur={onBlur}
        size={size}
        value={value}
        label={label}
        type={type}
      />
      {errorText && <small className={style.error}>{errorText}</small>}
    </FormControl>
  );
};

AuthInput.defaultProps = {
  color: "primary",
  disabled: false,
  error: false,
  fullWidth: false,
  size: "medium",
  errorText: undefined,
  endAdornment: null,
  iconClick: null,
  multiline: false,
  rows: 3,
};

export default AuthInput;
