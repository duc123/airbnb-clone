import React, { useState } from "react";
import * as yup from "yup";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useNavigate } from "react-router-dom";
import { VisibilityOffOutlined, VisibilityOutlined } from "@mui/icons-material";
import {
  selectLoginError,
  selectLoginLoading,
} from "../../redux/login/login.selector";
import { loginRequest } from "../../redux/login/login.actions";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import AuthInput from "../AuthInput";
import CustomizedButton from "../CustomizedButton";
import style from "./style.module.scss";
import CustomizedAlert from "../CustomizedAlert";

const LoginForm: React.FC<{}> = () => {
  const [passType, setPassType] = useState("password");
  const dispatch = useAppDispatch();
  const loading = useAppSelector(selectLoginLoading);
  const serverError = useAppSelector(selectLoginError);

  const navigate = useNavigate();

  const onChangePassType = () =>
    setPassType(() => (passType === "text" ? "password" : "text"));

  const defaultValues = {
    email: "",
    password: "",
  };

  const schema = yup.object({
    email: yup
      .string()
      .required("Email is required")
      .email("Please enter an invalid email"),
    password: yup.string().required("Password is required"),
  });

  const { handleSubmit, control } = useForm({
    resolver: yupResolver(schema),
    defaultValues,
    mode: "all",
  });

  const submit = async (data: any) => {
    const res = await dispatch(loginRequest(data));
    if (res.payload) navigate("/");
  };

  return (
    <div className={style.authForm}>
      <form onSubmit={handleSubmit(submit)}>
        {serverError && (
          <div style={{ marginBottom: "20px" }}>
            <CustomizedAlert
              sum="Login error"
              detail={serverError}
              color="error"
            />
          </div>
        )}

        <h3> Welcome to Airbnb</h3>
        <div className={style.formControl}>
          <Controller
            render={({
              field: { value, onChange, onBlur },
              fieldState: { error },
            }) => {
              return (
                <AuthInput
                  iconClick={null}
                  endAdornment=""
                  disabled={false}
                  // eslint-disable-next-line no-unneeded-ternary
                  error={error ? true : false}
                  value={value}
                  onChange={onChange}
                  onBlur={onBlur}
                  label="Email"
                  size="medium"
                  color="primary"
                  fullWidth
                  type="text"
                  errorText={error?.message}
                />
              );
            }}
            name="email"
            control={control}
          />
        </div>
        <div className={style.formControl}>
          <Controller
            render={({
              field: { value, onChange, onBlur },
              fieldState: { error },
            }) => {
              return (
                <AuthInput
                  iconClick={onChangePassType}
                  endAdornment={
                    passType === "text" ? (
                      <VisibilityOffOutlined />
                    ) : (
                      <VisibilityOutlined />
                    )
                  }
                  disabled={false}
                  // eslint-disable-next-line no-unneeded-ternary
                  error={error ? true : false}
                  value={value}
                  onChange={onChange}
                  onBlur={onBlur}
                  label="Password"
                  size="medium"
                  color="primary"
                  fullWidth
                  type={passType}
                  errorText={error?.message}
                />
              );
            }}
            name="password"
            control={control}
          />
        </div>
        <CustomizedButton
          onClick={null}
          href=""
          loading={loading}
          type="submit"
          fullwidth
          title="Login"
          disabled={loading}
          size="large"
          color="primary"
          variant="contained"
        />
      </form>
    </div>
  );
};

export default LoginForm;
