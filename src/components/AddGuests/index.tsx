import { AddOutlined, RemoveOutlined } from "@mui/icons-material";
import * as React from "react";
import { useSelector } from "react-redux";
import { useAppDispatch } from "../../app/hooks";
import { selectWho } from "../../redux/search/search.selector";
import { updateWho } from "../../redux/search/search.slice";
import style from "./style.module.scss";

const AddGuests = () => {
  const who = useSelector(selectWho);

  const dispatch = useAppDispatch();

  const { adults, children, infants, pets } = who;

  const plus = (name: "adults" | "pets" | "children" | "infants") => {
    return () => {
      const data = { ...who };
      data[name] += 1;
      dispatch(updateWho(data));
    };
  };

  const minus = (name: "adults" | "pets" | "children" | "infants") => {
    return () => {
      const data = { ...who };
      if (data[name] >= 1) {
        data[name] -= 1;
        dispatch(updateWho(data));
      }
    };
  };

  return (
    <div className={style.container}>
      <div className={style.box}>
        <div className={style.box_left}>
          <span className={style.title}>Adults</span>
          <span>Ages 13 or above</span>
        </div>
        <div className={style.box_right}>
          <span
            onClick={minus("adults")}
            className={`${style.calIcon} ${adults <= 0 ? style.inActive : ""}`}
          >
            <RemoveOutlined />
          </span>
          <span className={style.state}>{adults}</span>
          <span onClick={plus("adults")} className={style.calIcon}>
            <AddOutlined />
          </span>
        </div>
      </div>
      <div className={style.box}>
        <div className={style.box_left}>
          <span className={style.title}>Children</span>
          <span>Ages 2-12</span>
        </div>
        <div className={style.box_right}>
          <span
            onClick={minus("children")}
            className={`${style.calIcon} ${
              children <= 0 ? style.inActive : ""
            }`}
          >
            <RemoveOutlined />
          </span>
          <span className={style.state}>{children}</span>
          <span onClick={plus("children")} className={style.calIcon}>
            <AddOutlined />
          </span>
        </div>
      </div>
      <div className={style.box}>
        <div className={style.box_left}>
          <span className={style.title}>Infants</span>
          <span>Under 2</span>
        </div>
        <div className={style.box_right}>
          <span
            onClick={minus("infants")}
            className={`${style.calIcon} ${infants <= 0 ? style.inActive : ""}`}
          >
            <RemoveOutlined />
          </span>
          <span className={style.state}>{infants}</span>
          <span onClick={plus("infants")} className={style.calIcon}>
            <AddOutlined />
          </span>
        </div>
      </div>
      <div className={style.box}>
        <div className={style.box_left}>
          <span className={style.title}>Pets</span>
          <span>Dog, cat,...</span>
        </div>
        <div className={style.box_right}>
          <span
            onClick={minus("pets")}
            className={`${style.calIcon} ${pets <= 0 ? style.inActive : ""}`}
          >
            <RemoveOutlined />
          </span>
          <span className={style.state}>{pets}</span>
          <span onClick={plus("pets")} className={style.calIcon}>
            <AddOutlined />
          </span>
        </div>
      </div>
    </div>
  );
};

export default AddGuests;
