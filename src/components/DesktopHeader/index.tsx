import * as React from "react";
import { Link } from "react-router-dom";
import LinkOutlinedIcon from "@mui/icons-material/LinkOutlined";
import CustomizedButton from "../CustomizedButton";
import AccountMenu from "../AccountMenu";
import SearchBar from "../SearchBar";
import style from "./style.module.scss";
import CategoryList from "../CategoryList";
import useScrollPastOne from "../../hooks/useScrollPast1";

interface props {
  searchBarVisible?: boolean;
  filterVisible?: boolean;
}

const DesktopHeader = ({ searchBarVisible, filterVisible }: props) => {
  const [pastOne] = useScrollPastOne();

  return (
    <>
      <header className={`${style.header} `}>
        <div className={style.header_border}>
          <div className={style.container}>
            <div className={style.container_flex}>
              <div className={`${style.part} ${style.left}`}>
                <Link className={style.title} to="/">
                  <span>WeHome</span>
                </Link>
              </div>
              <div className={`${style.part} ${style.center}`}>
                {searchBarVisible && <SearchBar />}
              </div>
              <div className={`${style.part} ${style.right}`}>
                <CustomizedButton
                  onClick={null}
                  href="/become-a-host"
                  loading={false}
                  type="button"
                  fullwidth={false}
                  title="Become a host"
                  disabled={false}
                  size="medium"
                  color="inherit"
                  variant="text"
                />
                <AccountMenu />
              </div>
            </div>
          </div>
        </div>
      </header>
      {filterVisible && (
        <div className={`${pastOne ? style.past : ""} ${style.filters}`}>
          <div className={style.container}>
            <div className={style.categories}>
              <div className={style.categories_wrap}>
                <div className={style.categories_wrap_list}>
                  <CategoryList />
                </div>
              </div>
              <span className={style.filterBtn}>
                <LinkOutlinedIcon />
                <span>Filter</span>
              </span>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

DesktopHeader.defaultProps = {
  searchBarVisible: false,
  filterVisible: false,
};

export default DesktopHeader;
