import { IconButton, Modal } from "@mui/material";
import React from "react";
import CloseOutlinedIcon from "@mui/icons-material/CloseOutlined";
import style from "./style.module.scss";
import CustomizedButton from "../CustomizedButton";

type Props = {
  open: boolean;
  closeModal: Function;
  title: string;
  callback?: Function;
  children: React.ReactNode;
  loading?: boolean;
};

const ActionModal: React.FC<Props> = ({
  open,
  closeModal,
  title,
  callback,
  children,
  loading,
}) => {
  const handleClose = () => {
    closeModal();
  };

  const handleDone = () => {
    if (callback) callback();
  };

  return (
    <Modal open={open} onClose={handleClose}>
      <div className={style.content}>
        <div className={style.content_header}>
          <h3>{title}</h3>
          <IconButton onClick={handleClose} size="small">
            <CloseOutlinedIcon />
          </IconButton>
        </div>
        <div className={style.content_body}>{children}</div>
        <div className={style.content_footer}>
          <CustomizedButton
            size="large"
            onClick={handleDone}
            type="button"
            title="Done"
            loading={loading}
          />
        </div>
      </div>
    </Modal>
  );
};

ActionModal.defaultProps = {
  callback: () => {},
  loading: false,
};

export default ActionModal;
