import { LoadingButton } from "@mui/lab";
import * as React from "react";

interface propInterface {
  title: string;
  size?: "small" | "medium" | "large" | undefined;
  disabled?: boolean;
  color?:
    | "inherit"
    | "primary"
    | "secondary"
    | "success"
    | "error"
    | "info"
    | "warning"
    | undefined;
  variant?: "text" | "outlined" | "contained" | undefined;
  fullwidth?: boolean;
  type: "button" | "submit";
  loading?: boolean;
  href?: string;
  onClick?: Function | null;
}

const CustomizedButton = ({
  title,
  size,
  disabled,
  color,
  variant,
  fullwidth,
  type,
  loading,
  href,
  onClick,
}: propInterface) => {
  const click = () => {
    if (onClick) onClick();
  };

  return (
    <LoadingButton
      onClick={click}
      type={type}
      disableElevation
      fullWidth={fullwidth}
      color={color}
      disabled={disabled}
      size={size}
      variant={variant}
      loading={loading}
      href={href}
    >
      {title}
    </LoadingButton>
  );
};

CustomizedButton.defaultProps = {
  color: "primary",
  size: "medium",
  disabled: false,
  variant: "contained",
  fullwidth: false,
  loading: false,
  href: "",
  onClick: null,
};

export default CustomizedButton;
