import { ThemeProvider } from "@mui/material";
import * as React from "react";
import { Route, Routes } from "react-router-dom";
import theme from "./app/constants/theme";
import Wrapper from "./components/Wrapper";
import AuthGuard from "./guard/auth.guard";
import Account from "./pages/Account";
import PersonalInfo from "./pages/Account/Personalnfo";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register/indes";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Wrapper>
        <Routes>
          <Route element={<AuthGuard />}>
            <Route path="account">
              <Route path="personal-info" element={<PersonalInfo />} />
              <Route path="" element={<Account />} />
            </Route>
          </Route>

          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
        </Routes>
      </Wrapper>
    </ThemeProvider>
  );
}

export default App;
