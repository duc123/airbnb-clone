/* eslint-disable @typescript-eslint/no-unused-vars */
import * as React from "react";
import { useSelector } from "react-redux";
import moment from "moment";
import DatePicker from "react-date-picker";
import * as yup from "yup";
import { Checkbox, Chip, FormControlLabel, TextField } from "@mui/material";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import AuthInput from "../../../components/AuthInput";
import CustomizedButton from "../../../components/CustomizedButton";
import EditUserAvatar from "../../../components/EditUserAvatar";
import Header from "../../../components/Header";
import { User } from "../../../interfaces/user.interface";
import {
  selectAuthInfo,
  selectLoginLoading,
  selectLoginUid,
} from "../../../redux/login/login.selector";
import style from "./style.module.scss";
import { useAppDispatch } from "../../../app/hooks";
import { updateAuthInfo } from "../../../redux/login/login.actions";
import CustomizedSelect from "../../../components/CustomizedSelect";
import { AuthEditProps } from "../../../interfaces/authEditProp.interface";
import ActionModal from "../../../components/ActionModal";
import { languageList } from "../../../app/constants/languages";
import { selectCountries } from "../../../redux/util/util.selector";

const getBtnText = (
  key: string,
  current: string,
  authInfo: User | null,
): string => {
  if (current === key) return "Cancel";
  if (key === "legal name") {
    return "Edit";
  }
  if (key === "gender") {
    if (authInfo?.gender) return "Edit";
    return "Add";
  }
  if (key === "phoneNumber") {
    if (authInfo?.phoneNumber) return "Edit";
    return "Add";
  }
  if (key === "languages") {
    if (authInfo && authInfo?.languages.length > 0) return "Add more";
    return "Add";
  }
  if (key === "nationality") {
    if (authInfo?.nationality) return "Edit";
    return "Add";
  }
  if (key === "dateOfBirth") {
    if (authInfo && authInfo?.dateOfBirth) return "Edit";
    return "Add";
  }
  if (key === "about") {
    if (authInfo && authInfo?.about) return "Edit";
    return "Add";
  }

  return "";
};

const getClass = (key: string, current: string) => {
  let result = `${style.edit}`;
  if (current) {
    if (key === current) {
      result = `${style.edit} ${style.active}`;
    } else {
      result = `${style.edit} ${style.blur}`;
    }
  }

  return result;
};

const getBtnDisabled = (key: string, current: string) => {
  let result = false;
  if (current) {
    if (current === key) return result;
    result = true;
  }
  return result;
};

// Legal name

const EditLegalName = ({
  authInfo,
  current,
  changeCurrent,
  keyName,
}: AuthEditProps) => {
  const dispatch = useAppDispatch();
  const loading = useSelector(selectLoginLoading);

  const defaultValues = {
    firstname: authInfo?.firstName,
    lastname: authInfo?.lastName,
  };

  const schema = yup.object({
    firstname: yup.string().required(),
    lastname: yup.string().required(),
  });

  const { handleSubmit, control, reset, setValue } = useForm({
    resolver: yupResolver(schema),
    defaultValues,
    mode: "all",
  });

  const submit = async (data: any) => {
    if (authInfo?.id) {
      const values = { id: authInfo?.id, key: keyName, data };
      await dispatch(updateAuthInfo(values));
      changeCurrent("")();
    }
  };

  React.useEffect(() => {
    if (current === keyName) reset();
  }, [current]);

  return (
    <>
      <div className={style.edit_head}>
        <span className={style.label}>Legal name</span>
        <CustomizedButton
          onClick={changeCurrent(keyName)}
          variant="text"
          title={getBtnText(keyName, current, authInfo)}
          type="button"
          disabled={getBtnDisabled(keyName, current)}
        />
      </div>
      <div className={style.edit_body}>
        {current === keyName ? (
          <>
            <small>
              This is the name on your travel document, which could be a license
              or a passport.
            </small>
            <form onSubmit={handleSubmit(submit)}>
              <div className={`${style.formGroup} ${style.formControl}`}>
                <Controller
                  render={({
                    field: { value, onChange, onBlur },
                    fieldState: { error },
                  }) => {
                    return (
                      <AuthInput
                        value={value}
                        onChange={onChange}
                        onBlur={onBlur}
                        error={error ? true : false}
                        fullWidth
                        label="Firstname"
                        type="text"
                        errorText={error?.message}
                      />
                    );
                  }}
                  name="firstname"
                  control={control}
                />
                <Controller
                  render={({
                    field: { value, onChange, onBlur },
                    fieldState: { error },
                  }) => {
                    return (
                      <AuthInput
                        value={value}
                        onChange={onChange}
                        onBlur={onBlur}
                        error={error ? true : false}
                        fullWidth
                        label="Lastname"
                        type="text"
                        errorText={error?.message}
                      />
                    );
                  }}
                  name="lastname"
                  control={control}
                />
              </div>
              <CustomizedButton
                loading={loading}
                type="submit"
                title="Save"
                size="large"
              />
            </form>
          </>
        ) : (
          <span className={style.content}>
            {authInfo?.lastName} {authInfo?.firstName}
          </span>
        )}
      </div>
    </>
  );
};

// Gender

const EditGender = ({
  authInfo,
  current,
  changeCurrent,
  keyName,
}: AuthEditProps) => {
  const options = ["Male", "Female"];
  const schema = yup.object({
    gender: yup.string().required(),
  });
  const defaultValues = {
    gender: authInfo?.gender,
  };
  const { handleSubmit, control, reset, setValue } = useForm({
    resolver: yupResolver(schema),
    defaultValues,
    mode: "all",
  });

  const dispatch = useAppDispatch();

  const id = useSelector(selectLoginUid);

  const submit = async (data: any) => {
    if (id) {
      await dispatch(updateAuthInfo({ id, key: keyName, data }));
      await changeCurrent("")();
    }
  };

  const loading = useSelector(selectLoginLoading);

  React.useEffect(() => {
    if (current === keyName) {
      reset();
      setValue("gender", authInfo?.gender);
    }
  }, [current]);

  return (
    <>
      <div className={style.edit_head}>
        <span className={style.label}>Gender</span>
        <CustomizedButton
          onClick={changeCurrent(keyName)}
          variant="text"
          title={getBtnText(keyName, current, authInfo)}
          type="button"
          disabled={getBtnDisabled(keyName, current)}
        />
      </div>
      <div className={style.edit_body}>
        {current === keyName ? (
          <form onSubmit={handleSubmit(submit)}>
            <div className={style.formControl}>
              <Controller
                render={({
                  field: { value, onChange, onBlur },
                  fieldState: { error },
                }) => {
                  return (
                    <CustomizedSelect
                      onBlur={onBlur}
                      value={value}
                      onChange={onChange}
                      error={error ? true : false}
                      errorText={error?.message}
                      label="Gender"
                      fullWidth
                      options={options}
                    />
                  );
                }}
                name="gender"
                control={control}
              />
            </div>
            <CustomizedButton
              loading={loading}
              type="submit"
              title="Save"
              size="large"
            />
          </form>
        ) : (
          <span className={style.content}>
            {authInfo?.gender ? authInfo.gender : "Unspecified"}
          </span>
        )}
      </div>
    </>
  );
};

// Phone

const EditPhoneNumber = ({
  authInfo,
  current,
  changeCurrent,
  keyName,
}: AuthEditProps) => {
  const schema = yup.object({
    phoneNumber: yup.string().required(),
  });
  const defaultValues = {
    phoneNumber: authInfo?.phoneNumber,
  };
  const { handleSubmit, control, reset } = useForm({
    resolver: yupResolver(schema),
    defaultValues,
    mode: "all",
  });

  const dispatch = useAppDispatch();

  const id = useSelector(selectLoginUid);

  const submit = async (data: any) => {
    if (id) {
      await dispatch(updateAuthInfo({ id, key: keyName, data }));
      await changeCurrent("")();
    }
  };

  const loading = useSelector(selectLoginLoading);

  React.useEffect(() => {
    if (current === keyName) {
      reset();
    }
  }, [current]);

  return (
    <>
      <div className={style.edit_head}>
        <span className={style.label}>Phone number</span>
        <CustomizedButton
          onClick={changeCurrent(keyName)}
          variant="text"
          title={getBtnText(keyName, current, authInfo)}
          type="button"
          disabled={getBtnDisabled(keyName, current)}
        />
      </div>
      <div className={style.edit_body}>
        {current === keyName ? (
          <form onSubmit={handleSubmit(submit)}>
            <div className={style.formControl}>
              <Controller
                render={({
                  field: { value, onChange, onBlur },
                  fieldState: { error },
                }) => {
                  return (
                    <AuthInput
                      onBlur={onBlur}
                      value={value}
                      onChange={onChange}
                      error={error ? true : false}
                      errorText={error?.message}
                      label="Phone number"
                      fullWidth
                      type="text"
                    />
                  );
                }}
                name="phoneNumber"
                control={control}
              />
            </div>
            <CustomizedButton
              loading={loading}
              type="submit"
              title="Save"
              size="large"
            />
          </form>
        ) : (
          <span className={style.content}>
            {authInfo?.phoneNumber ? authInfo.phoneNumber : "Unspecified"}
          </span>
        )}
      </div>
    </>
  );
};

// Languages

const EditLanguages = ({
  authInfo,
  current,
  changeCurrent,
  keyName,
}: AuthEditProps) => {
  const [open, setOpen] = React.useState(false);

  const [chosenList, setChosenList] = React.useState<string[]>([]);

  const languages = languageList;

  const dispatch = useAppDispatch();
  const id = useSelector(selectLoginUid);
  const loading = useSelector(selectLoginLoading);

  const closeModal = () => {
    setOpen(() => false);
  };

  const openModal = () => {
    setOpen(() => true);
  };

  const handleClick = () => {
    changeCurrent(keyName)();
  };

  React.useEffect(() => {
    if (current === keyName) {
      openModal();
    } else {
      closeModal();
    }
  }, [current]);

  const getChecked = (code: string) => {
    const result = chosenList.includes(code);

    return result;
  };

  const handleChange = (code: string) => {
    return () => {
      const isChecked = getChecked(code);
      const list = [...chosenList];
      if (!isChecked) {
        list.push(code);
      } else {
        const idx = list.indexOf(code);
        list.splice(idx, 1);
      }

      setChosenList(list);
    };
  };

  const submit = async () => {
    if (id) {
      await dispatch(
        updateAuthInfo({ id, key: keyName, data: { languages: chosenList } }),
      );
      changeCurrent("")();
    }
  };

  React.useEffect(() => {
    if (authInfo && authInfo.languages) setChosenList([...authInfo.languages]);
  }, [authInfo, current]);

  const handleDelete = (code: string) => {
    return () => {
      if (!loading && authInfo && id) {
        const list = [...authInfo.languages];
        const idx = list?.indexOf(code);
        list?.splice(idx, 1);
        dispatch(
          updateAuthInfo({ id, key: keyName, data: { languages: list } }),
        );
      }
    };
  };

  const getLgName = (code: string) => {
    return languages.find((el) => el.code === code)?.name;
  };

  return (
    <>
      <ActionModal
        callback={submit}
        open={open}
        closeModal={changeCurrent("")}
        title="Add languages"
        loading={loading}
      >
        <div>
          <span className={style.lgDetail}>
            We have many international travelers who appreciate hosts who can
            speak their language.
          </span>
          <div className={style.lgList}>
            {languages.map((item) => (
              <FormControlLabel
                key={item.code}
                control={
                  <Checkbox
                    size="medium"
                    onChange={handleChange(item.code)}
                    checked={getChecked(item.code)}
                  />
                }
                label={item.name}
              />
            ))}
          </div>
        </div>
      </ActionModal>
      <div className={style.edit_head}>
        <span className={style.label}>Languages</span>
        <CustomizedButton
          onClick={handleClick}
          variant="text"
          title={getBtnText(keyName, current, authInfo)}
          type="button"
          disabled={getBtnDisabled(keyName, current)}
        />
      </div>
      <div className={style.edit_body}>
        <span className={style.content}>
          <span>Languages I speak</span>
          <div className={style.chosen}>
            {authInfo &&
              authInfo.languages.map((el) => (
                <Chip
                  key={el}
                  label={getLgName(el)}
                  variant="outlined"
                  onDelete={handleDelete(el)}
                />
              ))}
          </div>
        </span>
      </div>
    </>
  );
};

// Birth date
const EditBirthDate = ({
  authInfo,
  current,
  changeCurrent,
  keyName,
}: AuthEditProps) => {
  const schema = yup.object({
    dateOfBirth: yup.date().required(),
  });
  const defaultValues = {
    dateOfBirth: authInfo?.dateOfBirth
      ? new Date(authInfo.dateOfBirth)
      : undefined,
  };

  const { handleSubmit, control, reset } = useForm({
    resolver: yupResolver(schema),
    defaultValues,
    mode: "all",
  });

  const dispatch = useAppDispatch();

  const id = useSelector(selectLoginUid);

  const submit = async (data: any) => {
    if (id) {
      await dispatch(updateAuthInfo({ id, key: keyName, data }));
      await changeCurrent("")();
    }
  };

  const loading = useSelector(selectLoginLoading);

  React.useEffect(() => {
    if (current === keyName) {
      reset();
    }
  }, [current]);

  return (
    <>
      <div className={style.edit_head}>
        <span className={style.label}>Date of birth</span>
        <CustomizedButton
          onClick={changeCurrent(keyName)}
          variant="text"
          title={getBtnText(keyName, current, authInfo)}
          type="button"
          disabled={getBtnDisabled(keyName, current)}
        />
      </div>
      <div className={style.edit_body}>
        {current === keyName ? (
          <form onSubmit={handleSubmit(submit)}>
            <div className={style.formControl}>
              <Controller
                render={({
                  field: { value, onChange, onBlur },
                  fieldState: { error },
                }) => {
                  return (
                    <DatePicker
                      format="MM/dd/y"
                      className={style.caInput}
                      value={value}
                      onChange={onChange}
                    />
                  );
                }}
                name="dateOfBirth"
                control={control}
              />
            </div>
            <CustomizedButton
              loading={loading}
              type="submit"
              title="Save"
              size="large"
            />
          </form>
        ) : (
          <span className={style.content}>
            {authInfo?.dateOfBirth
              ? moment(authInfo.dateOfBirth).format("MMM Do YYYY")
              : "Unspecified"}
          </span>
        )}
      </div>
    </>
  );
};

const EditNationality = ({
  authInfo,
  current,
  changeCurrent,
  keyName,
}: AuthEditProps) => {
  const [options, setOptions] = React.useState<string[]>([]);

  const countries = useSelector(selectCountries);

  const schema = yup.object({
    nationality: yup.string().required(),
  });
  const defaultValues = {
    nationality: authInfo?.nationality,
  };
  const { handleSubmit, control, reset, setValue } = useForm({
    resolver: yupResolver(schema),
    defaultValues,
    mode: "all",
  });

  const dispatch = useAppDispatch();

  const id = useSelector(selectLoginUid);

  const submit = async (data: any) => {
    if (id) {
      await dispatch(updateAuthInfo({ id, key: keyName, data }));
      await changeCurrent("")();
    }
  };

  const loading = useSelector(selectLoginLoading);

  React.useEffect(() => {
    if (current === keyName) {
      reset();
      setValue("nationality", authInfo?.nationality);
    }
  }, [current]);

  React.useEffect(() => {
    if (countries.length > 0) {
      setOptions(countries);
    }
  }, [countries]);

  return (
    <>
      <div className={style.edit_head}>
        <span className={style.label}>Nationality</span>
        <CustomizedButton
          onClick={changeCurrent(keyName)}
          variant="text"
          title={getBtnText(keyName, current, authInfo)}
          type="button"
          disabled={getBtnDisabled(keyName, current)}
        />
      </div>
      <div className={style.edit_body}>
        {current === keyName ? (
          <form onSubmit={handleSubmit(submit)}>
            <div className={style.formControl}>
              <Controller
                render={({
                  field: { value, onChange, onBlur },
                  fieldState: { error },
                }) => {
                  return (
                    <CustomizedSelect
                      onBlur={onBlur}
                      value={value}
                      onChange={onChange}
                      error={error ? true : false}
                      errorText={error?.message}
                      label="Select nationality"
                      fullWidth
                      options={options}
                    />
                  );
                }}
                name="nationality"
                control={control}
              />
            </div>
            <CustomizedButton
              loading={loading}
              type="submit"
              title="Save"
              size="large"
            />
          </form>
        ) : (
          <span className={style.content}>
            {authInfo?.nationality ? authInfo.nationality : "Unspecified"}
          </span>
        )}
      </div>
    </>
  );
};

const EditAbout = ({
  authInfo,
  current,
  changeCurrent,
  keyName,
}: AuthEditProps) => {
  const schema = yup.object({
    about: yup.string().required(),
  });
  const defaultValues = {
    about: authInfo?.about,
  };
  const { handleSubmit, control, reset } = useForm({
    resolver: yupResolver(schema),
    defaultValues,
    mode: "all",
  });

  const dispatch = useAppDispatch();

  const id = useSelector(selectLoginUid);

  const submit = async (data: any) => {
    if (id) {
      await dispatch(updateAuthInfo({ id, key: keyName, data }));
      await changeCurrent("")();
    }
  };

  const loading = useSelector(selectLoginLoading);

  React.useEffect(() => {
    if (current === keyName) {
      reset();
    }
  }, [current]);

  return (
    <>
      <div className={style.edit_head}>
        <span className={style.label}>About</span>
        <CustomizedButton
          onClick={changeCurrent(keyName)}
          variant="text"
          title={getBtnText(keyName, current, authInfo)}
          type="button"
          disabled={getBtnDisabled(keyName, current)}
        />
      </div>
      <div className={style.edit_body}>
        {current === keyName ? (
          <form onSubmit={handleSubmit(submit)}>
            <div className={style.formControl}>
              <Controller
                render={({
                  field: { value, onChange, onBlur },
                  fieldState: { error },
                }) => {
                  return (
                    <AuthInput
                      rows={5}
                      multiline
                      onBlur={onBlur}
                      value={value}
                      onChange={onChange}
                      error={error ? true : false}
                      errorText={error?.message}
                      label="About"
                      fullWidth
                      type="text"
                    />
                  );
                }}
                name="about"
                control={control}
              />
            </div>
            <CustomizedButton
              loading={loading}
              type="submit"
              title="Save"
              size="large"
            />
          </form>
        ) : (
          <span className={style.content}>
            {authInfo?.about ? authInfo.about : "Unspecified"}
          </span>
        )}
      </div>
    </>
  );
};

const PersonalInfo = () => {
  const authInfo = useSelector(selectAuthInfo);

  const [current, setCurrent] = React.useState("");

  const changeCurrent = (key: string) => {
    if (current)
      return function () {
        setCurrent(() => "");
      };
    return function () {
      setCurrent(() => key);
    };
  };

  return (
    <>
      <Header />
      <div className="appContainer">
        <div className={style.container}>
          <div className={style.head}>
            <h1>Personal info</h1>
          </div>

          <div className={style.main}>
            <div className={style.main_left}>
              <div>
                {authInfo && (
                  <>
                    <div className={getClass("legal name", current)}>
                      <EditLegalName
                        authInfo={authInfo}
                        current={current}
                        changeCurrent={changeCurrent}
                        keyName="legal name"
                      />
                    </div>
                    <div className={getClass("gender", current)}>
                      <EditGender
                        authInfo={authInfo}
                        current={current}
                        changeCurrent={changeCurrent}
                        keyName="gender"
                      />
                    </div>
                    <div className={getClass("phoneNumber", current)}>
                      <EditPhoneNumber
                        authInfo={authInfo}
                        current={current}
                        changeCurrent={changeCurrent}
                        keyName="phoneNumber"
                      />
                    </div>
                    <div className={getClass("languages", current)}>
                      <EditLanguages
                        authInfo={authInfo}
                        current={current}
                        changeCurrent={changeCurrent}
                        keyName="languages"
                      />
                    </div>
                    <div className={getClass("nationality", current)}>
                      <EditNationality
                        authInfo={authInfo}
                        current={current}
                        changeCurrent={changeCurrent}
                        keyName="nationality"
                      />
                    </div>
                    <div className={getClass("dateOfBirth", current)}>
                      <EditBirthDate
                        authInfo={authInfo}
                        current={current}
                        changeCurrent={changeCurrent}
                        keyName="dateOfBirth"
                      />
                    </div>
                    <div className={getClass("about", current)}>
                      <EditAbout
                        authInfo={authInfo}
                        current={current}
                        changeCurrent={changeCurrent}
                        keyName="about"
                      />
                    </div>
                  </>
                )}
              </div>
            </div>
            <div className={style.main_right}>
              <div className={style.main_right_box}>
                <EditUserAvatar />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default PersonalInfo;
