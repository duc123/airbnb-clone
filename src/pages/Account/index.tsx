import * as React from "react";
import AnalyticsOutlinedIcon from "@mui/icons-material/AnalyticsOutlined";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { AccountPage, accountPages } from "../../app/constants/accountPages";
import Header from "../../components/Header";
import style from "./style.module.scss";
import { selectAuthInfo } from "../../redux/login/login.selector";

const Account = () => {
  const list: AccountPage[] = accountPages;

  const info = useSelector(selectAuthInfo);

  const navigate = useNavigate();

  const goTo = (url: string) => {
    return function () {
      navigate(`/account/${url}`);
    };
  };

  return (
    <div className="appContainer">
      <Header />

      <div className={style.container}>
        <div className={style.head}>
          <h1>Account</h1>
          <p>
            <strong>
              {info?.lastName} {info?.firstName},
            </strong>
            <span> </span>
            <span>{info?.email}</span>
          </p>
        </div>

        <div className={style.list}>
          {list.map((item) => (
            <div onClick={goTo(item.url)} key={item.id} className={style.item}>
              <span className={style.icon}>
                <AnalyticsOutlinedIcon />
              </span>
              <p className={style.title}>{item.title}</p>
              <span className={style.detail}>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid
                dolorem consectetur consequuntur nam.
              </span>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Account;
