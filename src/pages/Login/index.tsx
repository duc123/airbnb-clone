import * as React from "react";
import { useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import Header from "../../components/Header";
import LoginForm from "../../components/LoginForm";
import { selectLoginUid } from "../../redux/login/login.selector";
import style from "./style.module.scss";

const Login = () => {
  const uid = useSelector(selectLoginUid);

  const navigate = useNavigate();

  React.useEffect(() => {
    if (uid) navigate("/");
  }, [uid]);

  return (
    <>
      <Header searchBarVisible={false} />
      <div className={style.auth}>
        <div className={style.container}>
          <div className={style.container_head}>
            <h2>Login</h2>
          </div>
          <div className={style.container_body}>
            <LoginForm />
            <span className={style.redirect}>
              Do not have an account? <Link to="/register">Register</Link>
            </span>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
