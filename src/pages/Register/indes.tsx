import * as React from "react";
import { useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import Header from "../../components/Header";
import RegisterForm from "../../components/RegisterForm";
import { selectLoginUid } from "../../redux/login/login.selector";
import style from "./style.module.scss";

const Register = () => {
  const uid = useSelector(selectLoginUid);

  const navigate = useNavigate();

  React.useEffect(() => {
    if (uid) navigate("/");
  }, [uid]);
  return (
    <>
      <Header searchBarVisible={false} />
      <div className={style.auth}>
        <div className={style.container}>
          <div className={style.container_head}>
            <h2>Register</h2>
          </div>
          <div className={style.container_body}>
            <RegisterForm />
            <span className={style.redirect}>
              Already have an account? <Link to="/login">Login</Link>
            </span>
          </div>
        </div>
      </div>
    </>
  );
};

export default Register;
