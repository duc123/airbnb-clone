import { createTheme } from "@mui/material";

declare module "@mui/material/styles" {
  interface PaletteOptions {
    neutral: PaletteOptions["primary"];
  }
}
const theme = createTheme({
  typography: {
    fontFamily: "Open Sans, sans-serif",
  },
  palette: {
    primary: {
      main: "#0066ff",
      dark: "#0052cc",
    },
    error: {
      main: "#ef4444",
      dark: "#dc2626",
    },
    neutral: {
      main: "#ef4444",
      dark: "#dc2626",
    },
  },
});

export default theme;
