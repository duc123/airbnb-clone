export interface AccountPage {
  title: string;
  url: string;
  id: number;
}

export const accountPages: AccountPage[] = [
  {
    title: "Personal info",
    url: "personal-info",
    id: 1,
  },
  {
    title: "Login & security",
    url: "login-security",
    id: 2,
  },
  {
    title: "Payment",
    url: "payment",
    id: 3,
  },
  {
    title: "Notifications",
    url: "notification",
    id: 4,
  },
  {
    title: "Hosting tools",
    url: "tool",
    id: 5,
  },
  {
    title: "Privacy",
    url: "privacy",
    id: 6,
  },
  {
    title: "Currency",
    url: "currency",
    id: 7,
  },
  {
    title: "For work",
    url: "for-work",
    id: 8,
  },
  {
    title: "Referral credit",
    url: "credit",
    id: 9,
  },
];
