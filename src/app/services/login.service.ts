import { signInWithEmailAndPassword, signOut } from "firebase/auth";
import { auth } from "../../firebase/config";

export const login = async (email: string, password: string) => {
  try {
    const res = await signInWithEmailAndPassword(auth, email, password);
    return res.user.uid;
  } catch (err) {
    throw err;
  }
};

export const logout = async () => {
  try {
    await signOut(auth);
    return true;
  } catch (err) {
    throw err;
  }
};
