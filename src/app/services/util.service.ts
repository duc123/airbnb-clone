export const getCountries = async () => {
  try {
    const res = await fetch("https://restcountries.com/v3.1/all");
    const json = await res.json();
    if (json.length > 0) {
      return json.map((el: any) => el.name.common);
    }
    return [];
  } catch (err) {
    throw err;
  }
};

export const searchLocationInput = async (input: string) => {
  try {
    const res = await fetch(
      `https://www.airbnb.com/api/v2/autocompletes?country=VN&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&language=en&locale=en&num_results=5&user_input=${input}&api_version=1.2.0&satori_config_token=EhIiQRIiIjISEjISIRESIlFCNcIDFQYVAhUCBYoBAgA&vertical_refinement=homes&region=-1&options=should_filter_by_vertical_refinement%7Chide_nav_results%7Cshould_show_stays%7Csimple_search%7Cflex_destinations_june_2021_launch_web_treatment`,
    );
    const json = await res.json();
    return json;
  } catch (err) {
    throw err;
  }
};
