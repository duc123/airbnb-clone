import { createUserWithEmailAndPassword } from "firebase/auth";
import { doc, setDoc } from "firebase/firestore";
import { auth, db } from "../../firebase/config";
import { User } from "../../interfaces/user.interface";

export const register = async (
  email: string,
  password: string,
  firstName: string,
  lastName: string,
) => {
  try {
    const res = await createUserWithEmailAndPassword(auth, email, password);
    const id = res.user.uid;

    const user: User = {
      id,
      email,
      firstName,
      lastName,
      gender: "",
      phoneNumber: "",
      govermentId: "",
      address: "",
      avatar: "",
      about: "",
      isActive: true,
      dateOfBirth: "",
      nationality: "",
      languages: [],
      location: "",
      work: "",
      created_at: new Date(),
    };

    await setDoc(doc(db, "users", id), {
      ...user,
    });

    return id;
  } catch (err) {
    throw err;
  }
};
