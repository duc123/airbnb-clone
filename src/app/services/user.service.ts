import { doc, getDoc, updateDoc } from "firebase/firestore";
import {
  deleteObject,
  getDownloadURL,
  ref,
  uploadBytesResumable,
} from "firebase/storage";
import { db, storage } from "../../firebase/config";
import { User } from "../../interfaces/user.interface";

export const getUser = async (param: string) => {
  try {
    const docRef = doc(db, "users", param);
    const res = await getDoc(docRef);

    if (res.exists()) {
      const data = res.data();
      const {
        id,
        email,
        firstName,
        lastName,
        gender,
        phoneNumber,
        govermentId,
        address,
        avatar,
        about,
        isActive,
        dateOfBirth,
        nationality,
        languages,
        location,
        work,
        created_at,
      } = data;
      const user: User = {
        id,
        email,
        firstName,
        lastName,
        gender,
        phoneNumber,
        govermentId,
        address,
        avatar,
        about,
        isActive,
        dateOfBirth: dateOfBirth ? dateOfBirth.toDate().toISOString() : "",
        nationality,
        languages,
        location,
        work,
        created_at: created_at.toDate().toISOString(),
      };

      return user;
    }
    return null;
  } catch (err) {
    throw err;
  }
};

export const updateAuth = async (id: string, key: string, data: any) => {
  try {
    const docRef = doc(db, "users", id);

    const updatePart: any = {};

    if (key === "legal name") {
      const { firstname, lastname } = data;
      updatePart.firstName = firstname;
      updatePart.lastName = lastname;
    }

    if (key === "gender") {
      const { gender } = data;
      updatePart.gender = gender;
    }

    if (key === "phoneNumber") {
      const { phoneNumber } = data;
      updatePart.phoneNumber = phoneNumber;
    }

    if (key === "languages") {
      const { languages } = data;
      updatePart.languages = languages;
    }

    if (key === "nationality") {
      const { nationality } = data;
      updatePart.nationality = nationality;
    }

    if (key === "dateOfBirth") {
      const { dateOfBirth } = data;
      updatePart.dateOfBirth = dateOfBirth;
    }

    if (key === "about") {
      const { about } = data;
      updatePart.about = about;
    }

    const res = await updateDoc(docRef, updatePart);
    return res;
  } catch (err) {
    throw err;
  }
};

const dropFile = async (filename: string | undefined) => {
  if (filename) {
    const fileRef = ref(storage, `${filename}`);
    try {
      const res = await deleteObject(fileRef);
      return res;
    } catch (err) {
      throw err;
    }
  } else {
    return true;
  }
};

export const uploadAvatarFile = (file: File, oldAvatar: string | undefined) => {
  return new Promise((resolve, reject) => {
    dropFile(oldAvatar)
      .then(() => {
        const { name } = file;
        const date = new Date().toISOString();
        const storageRef = ref(storage, `avatars/${name + date}`);
        const upload = uploadBytesResumable(storageRef, file, {
          contentType: "image/jpeg",
        });
        upload.on(
          "state_changed",
          (snapshot) => {
            const progress =
              (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log(progress);
          },
          (err) => {
            reject(err);
          },
          () => {
            getDownloadURL(upload.snapshot.ref).then((url) => {
              resolve(url);
            });
          },
        );
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

export const updateAvatar = async (id: string, url: string) => {
  try {
    const docRef = doc(db, "users", id);
    const res = await updateDoc(docRef, {
      avatar: url,
    });

    return res;
  } catch (err) {
    throw err;
  }
};
