import { RootState } from "../store";

export const selectLoginLoading = (state: RootState) => state.login.loading;
export const selectLoginUid = (state: RootState) => state.login.uid;
export const selectLoginError = (state: RootState) => state.login.error;
export const selectAuthInfo = (state: RootState) => state.login.info;
export const selectAvatarLoading = (state: RootState) =>
  state.login.avatarLoading;
