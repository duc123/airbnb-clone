import { createAsyncThunk } from "@reduxjs/toolkit";
import { login, logout } from "../../app/services/login.service";
import {
  getUser,
  updateAuth,
  updateAvatar,
  uploadAvatarFile,
} from "../../app/services/user.service";

export const loginRequest = createAsyncThunk(
  "login-request",
  async (data: { email: string; password: string }) => {
    const { email, password } = data;
    try {
      const res = await login(email, password);
      return res;
    } catch (err) {
      throw err;
    }
  },
);

export const logoutRequest = createAsyncThunk("logout-request", async () => {
  try {
    const res = await logout();
    return res;
  } catch (err) {
    throw err;
  }
});

export const getAuthInfo = createAsyncThunk(
  "get-auth-info",
  async (id: string) => {
    try {
      const res = await getUser(id);
      console.log(res);

      return res;
    } catch (err) {
      throw err;
    }
  },
);

export const updateAuthInfo = createAsyncThunk(
  "update-auth-info",
  async (param: { id: string; key: string; data: any }, dispatch) => {
    const { id, key, data } = param;
    try {
      const res = await updateAuth(id, key, data);
      await dispatch.dispatch(getAuthInfo(id));
      return res;
    } catch (err) {
      throw err;
    }
  },
);

export const updateAuthAvatar = createAsyncThunk(
  "update-auth-avatar",
  async (
    param: { id: string; file: File; oldAvatar: string | undefined },
    dispatch,
  ) => {
    const { id, file, oldAvatar } = param;
    try {
      const url = (await uploadAvatarFile(file, oldAvatar)) as string;
      await updateAvatar(id, url);
      await dispatch.dispatch(getAuthInfo(id));
      return true;
    } catch (err) {
      throw err;
    }
  },
);
