import { createSlice } from "@reduxjs/toolkit";
import {
  getAuthInfo,
  loginRequest,
  logoutRequest,
  updateAuthAvatar,
  updateAuthInfo,
} from "./login.actions";
import { loginInitialState, loginState } from "./login.state";

const saveUid = (uid: string) => {
  localStorage.setItem("uid", JSON.stringify(uid));
};

const removeUid = () => {
  localStorage.removeItem("uid");
};

const loginSlice = createSlice({
  name: "login",
  initialState: loginInitialState,
  reducers: {
    hideLoginError: (state: loginState) => {
      state.error = "";
    },
    loginSuccess: (state: loginState, action) => {
      saveUid(action.payload);
      state.uid = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(loginRequest.pending, (state) => {
      state.error = "";
      state.loading = true;
    });
    builder.addCase(loginRequest.fulfilled, (state, action) => {
      state.loading = false;
      state.uid = action.payload;
      saveUid(action.payload);
    });
    builder.addCase(loginRequest.rejected, (state, action) => {
      const { error } = action;
      state.error = error.message;
      state.loading = false;
    });

    builder.addCase(logoutRequest.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(logoutRequest.fulfilled, (state) => {
      state.loading = false;
      state.uid = "";
      removeUid();
    });
    builder.addCase(logoutRequest.rejected, (state, action) => {
      state.loading = false;
      console.log(action.error);
    });

    builder.addCase(getAuthInfo.fulfilled, (state, action) => {
      state.info = action.payload;
    });

    //

    builder.addCase(updateAuthInfo.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(updateAuthInfo.fulfilled, (state) => {
      state.loading = false;
    });
    builder.addCase(updateAuthInfo.rejected, (state) => {
      state.loading = false;
    });

    builder.addCase(updateAuthAvatar.pending, (state) => {
      state.avatarLoading = true;
    });
    builder.addCase(updateAuthAvatar.fulfilled, (state) => {
      state.avatarLoading = false;
    });
    builder.addCase(updateAuthAvatar.rejected, (state) => {
      state.avatarLoading = false;
    });
  },
});

export const {
  reducer: loginReducer,
  actions: { hideLoginError, loginSuccess },
} = loginSlice;
