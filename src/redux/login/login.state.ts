import { User } from "../../interfaces/user.interface";

export interface loginState {
  loading: boolean;
  uid: string | undefined;
  error: string | undefined;
  info: User | null;
  avatarLoading: boolean;
}

const token = localStorage.getItem("uid") ? localStorage.getItem("uid") : "";

export const loginInitialState: loginState = {
  loading: false,
  uid: token ? JSON.parse(token) : "",
  error: "",
  info: null,
  avatarLoading: false,
};
