export interface registerState {
  loading: boolean;
  error: string | undefined;
}

export const registerInitialState: registerState = {
  loading: false,
  error: "",
};
