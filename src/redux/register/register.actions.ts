import { createAsyncThunk } from "@reduxjs/toolkit";
import { register } from "../../app/services/register.service";
import { loginSuccess } from "../login/login.slice";

export const registerRequest = createAsyncThunk(
  "register-request",
  async (
    data: {
      email: string;
      password: string;
      firstname: string;
      lastname: string;
    },
    dispatch,
  ) => {
    const { email, password, firstname, lastname } = data;
    try {
      const res = await register(email, password, firstname, lastname);
      dispatch.dispatch(loginSuccess(res));
      return res;
    } catch (err) {
      throw err;
    }
  },
);
