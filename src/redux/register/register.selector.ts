import { RootState } from "../store";

export const selectRegisterLoading = (state: RootState) =>
  state.register.loading;
export const selectRegisterError = (state: RootState) => state.register.error;
