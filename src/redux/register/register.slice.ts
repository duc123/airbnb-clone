import { createSlice } from "@reduxjs/toolkit";
import { registerRequest } from "./register.actions";
import { registerInitialState, registerState } from "./register.state";

const registerSlice = createSlice({
  name: "register",
  initialState: registerInitialState,
  reducers: {
    hideRegisterError: (state: registerState) => {
      state.error = "";
    },
  },
  extraReducers: (builder) => {
    builder.addCase(registerRequest.pending, (state) => {
      state.error = "";
      state.loading = true;
    });
    builder.addCase(registerRequest.fulfilled, (state, action) => {
      console.log(action);
      state.loading = false;
    });
    builder.addCase(registerRequest.rejected, (state, action) => {
      const { error } = action;
      state.error = error.message;
      state.loading = false;
    });
  },
});

export const {
  reducer: registerReducer,
  actions: { hideRegisterError },
} = registerSlice;
