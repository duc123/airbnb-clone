import { RootState } from "../store";

export const selectWhere = (state: RootState) => state.search.where;
export const selectCheckin = (state: RootState) => state.search.checkIn;
export const selectCheckout = (state: RootState) => state.search.checkOut;
export const selectWho = (state: RootState) => state.search.who;
