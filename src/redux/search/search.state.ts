export interface searchState {
  where: string;
  checkIn: string;
  checkOut: string;
  who: {
    adults: number;
    children: number;
    infants: number;
    pets: number;
  };
}

export const searchInitialState: searchState = {
  where: "",
  checkIn: "",
  checkOut: "",
  who: {
    adults: 0,
    children: 0,
    infants: 0,
    pets: 0,
  },
};
