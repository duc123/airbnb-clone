import { createSlice } from "@reduxjs/toolkit";
import { searchInitialState } from "./search.state";

const searchSlice = createSlice({
  name: "search",
  initialState: searchInitialState,
  reducers: {
    updateWhere: (state, action) => {
      state.where = action.payload;
    },
    updateCheckIn: (state, action) => {
      state.checkIn = action.payload;
    },
    updateCheckOut: (state, action) => {
      state.checkOut = action.payload;
    },
    updateWho: (state, action) => {
      state.who = action.payload;
    },
  },
});

export const {
  reducer: searchReducer,
  actions: { updateWhere, updateCheckIn, updateCheckOut, updateWho },
} = searchSlice;
