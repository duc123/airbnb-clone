import { createSlice } from "@reduxjs/toolkit";
import { getCountryList } from "./util.actions";
import { utilInitialState, utilState } from "./util.state";

const utilSlice = createSlice({
  name: "util",
  initialState: utilInitialState,
  reducers: {
    openSearch: (state: utilState) => {
      state.searchActive = true;
    },
    closeSearch: (state: utilState) => {
      state.searchActive = false;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getCountryList.fulfilled, (state, action) => {
      state.countries = action.payload;
    });
  },
});

export const {
  reducer: utilReducer,
  actions: { openSearch, closeSearch },
} = utilSlice;
