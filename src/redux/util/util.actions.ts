import { createAsyncThunk } from "@reduxjs/toolkit";
import { getCountries } from "../../app/services/util.service";

export const getCountryList = createAsyncThunk("get-country-list", async () => {
  try {
    const res = await getCountries();
    return res;
  } catch (err) {
    console.log(err);

    throw err;
  }
});
