import { RootState } from "../store";

export const selectCountries = (state: RootState) => state.util.countries;
export const selectSearchActive = (state: RootState) => state.util.searchActive;
