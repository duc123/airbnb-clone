export interface utilState {
  countries: string[];
  searchActive: boolean;
}

export const utilInitialState: utilState = {
  countries: [],
  searchActive: false,
};
