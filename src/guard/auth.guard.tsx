import * as React from "react";
import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";
import { selectLoginUid } from "../redux/login/login.selector";

const AuthGuard = () => {
  const uid = useSelector(selectLoginUid);

  return uid ? <Outlet /> : <Navigate replace to="/login" />;
};

export default AuthGuard;
