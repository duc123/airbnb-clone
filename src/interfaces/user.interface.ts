export interface User {
  id?: string;
  email: string;
  firstName: string;
  lastName: string;
  gender: string;
  phoneNumber: string;
  govermentId: string;
  address: string;
  avatar: string;
  about: string;
  isActive: boolean;
  dateOfBirth: any;
  nationality: string;
  languages: string[];
  location: string;
  work: string;
  created_at: any;
}
