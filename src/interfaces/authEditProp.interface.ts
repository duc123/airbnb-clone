import { User } from "./user.interface";

export interface AuthEditProps {
  authInfo: User | null;
  current: string;
  changeCurrent: Function;
  keyName: string;
}
