import { useEffect, useState } from "react";

const useScrollPastOne = () => {
  const [pastOne, setPastOne] = useState(false);

  const handleScroll = () => {
    setPastOne(() => window.scrollY > 0);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  return [pastOne, setPastOne];
};

export default useScrollPastOne;
